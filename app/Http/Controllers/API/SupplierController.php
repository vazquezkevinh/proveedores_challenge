<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Supplier;
use App\Models\User;
use Illuminate\Http\Request;
use Storage;

class SupplierController extends Controller
{
    private $error = false;
    
    /**
     * Retrieve all suppliers.
     *
     * @return Object
     */
    public function index()
    {
        //$suppliers = Supplier::all()->sortByDesc('sync')->toArray();
        $suppliers = Supplier::all()->toArray();
        // return array_reverse($suppliers);
        return array_reverse($suppliers);
    }


    /**
     * Import all suppliers from a .txt file 
     *
     * @return json
     */
    public function import(Request $request)
    {
        //Prevent errors
        try{
            //Validate input
            $validated = $request->validate([
                'file' => 'required|file|mimetypes:text/plain',
            ]);

            //Store file in public storage path
            $uploaded = Storage::disk('public')->put('suppliers.txt', file_get_contents($request->file('file')) );
            $file = Storage::disk('public')->path('suppliers.txt');

            //Validate if the file was saved correctly
            if($uploaded && !$file){
                return response()->json([
                    'valido' => false,
                    'msg' => 'El archivo no pudo ser cargado'
                ]);
            }

            //Variable to store all data that can not being saved
            $failedSuppliers = [];

            //iterate all suppliers from the .txt
            foreach ($this->file_lines($file) as $key=>$line){

                //Put each line on the buffer in order to iterate and save it
                $buffer = str_replace(array("\r", "\n"), '', $line);
                $array[$key] = explode('|', $buffer);
                
                if(isset($array[$key][0]) && isset($array[$key][1]) && isset($array[$key][2])){
                    //convert rfc into uppercase
                    $rfc = strtoupper($array[$key][1]);
                    
                    //convert email into lowercase
                    $email = strtolower($array[$key][2]);

                    //Make an array to store it
                    $supplier = [
                        'name' => $array[$key][0],
                        'rfc' => $rfc,
                        'email' => $email,
                        'sync' => null,
                    ];
                    
                    try{
                        //Storing the supplier
                        Supplier::create($supplier);
                    }catch(\Exception $e){
                        $this->error = true;

                        //add the failed supplier to an array
                        array_push($failedSuppliers, $supplier);
                    }
                }

            }

            return response()->json([
                'status' => true,
                'msg' => 'Los proveedores fueron sincronizados correctamente',
                'error' => $this->error,
                'suppliers_not_sync' => $failedSuppliers
            ], 200);

        }catch(\Exception $e){
            $this->error = true;
            return response()->json([
                'status' => false,
                'msg' => 'Ocurrió un error al sincronizar los proveedores',
                'error' => $this->error,
                'error_msg' => $e->getMessage(),
            ], 500);
        }

    }

    /**
     * Integrate supplier applicant
     *
     * @return json
     */
    public function supplierIntegrate(Request $request, $id)
    {
        try{
            if($supplier = Supplier::find($id)){

                \DB::beginTransaction();

                $user = User::create([
                    'name'     => $supplier->name,
                    'email'    => $supplier->email,
                    'rfc'      => $supplier->rfc,
                    'password' => \Hash::make($supplier->rfc),
                    'role'     => 'supplier',
                    'status'   => 1,
                ]);

                if($user){
                    // sync 0 means supplier had been accepted
                    $supplier->sync = 0;
                    $supplier->save();

                    \DB::commit();
    
                    return response()->json([
                        'status' => true,
                        'msg' => 'El proveedor ha sido integrado correctamente',
                        'error' => $this->error,
                    ], 200);

                }else{
                    return response()->json([
                        'status' => false,
                        'msg' => 'El proveedor no pudo ser integrado',
                        'error' => $this->error,
                    ], 200);
                }
            }else{
                //Not found
                $this->error = false;
                return response()->json([
                    'status' => false,
                    'msg' => 'El proveedor no fue localizado',
                    'error' => $this->error,
                ], 200);
            }
        }catch(\Exception $e){
            \DB::rollback();
            $this->error = true;
            return response()->json([
                'status' => false,
                'msg' => 'Ocurrió un error al integrar al proveedor',
                'error' => $this->error,
                'error_msg' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Deny supplier integration
     *
     * @return json
     */
    public function rejectSupplier(Request $request, $id)
    {
        try{
            if($supplier = Supplier::find($id)){

                // sync 1 means supplier had been rejected
                $supplier->sync = 1;
                $supplier->save();

                return response()->json([
                    'status' => true,
                    'msg' => 'El proveedor ha sido rechazado',
                    'error' => $this->error,
                ], 200);

            }else{
                //Not found
                $this->error = false;
                return response()->json([
                    'status' => false,
                    'msg' => 'El proveedor no fue localizado',
                    'error' => $this->error,
                ], 200);
            }
        }catch(\Exception $e){
            \DB::rollback();
            $this->error = true;
            return response()->json([
                'status' => false,
                'msg' => 'Ocurrió un error al actualizar proveedor',
                'error' => $this->error,
                'error_msg' => $e->getMessage(),
            ], 500);
        }
    }

    //iterate data from large files
    public function file_lines($filename) {
        $file = fopen($filename, 'r');
        while (($line = fgets($file)) !== false) {
            yield $line;
        }
        fclose($file);
    }
}