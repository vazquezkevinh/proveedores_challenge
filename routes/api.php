<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Http\Controllers\API\SupplierController;
use App\Http\Controllers\API\UserController;
use Illuminate\Support\Facades\Route;

Route::post('login', [UserController::class, 'login']);
Route::post('logout', [UserController::class, 'logout'])->middleware('auth:sanctum');

Route::group(['prefix' => 'suppliers', 'middleware' => 'auth:sanctum'], function () {
    Route::get('/', [SupplierController::class, 'index']);
    Route::post('/import', [SupplierController::class, 'import']);
    Route::post('integrate/{id}', [SupplierController::class, 'supplierIntegrate']);
    Route::post('reject/{id}', [SupplierController::class, 'rejectSupplier']);
});