<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comments('Nombre completo del proveedor')->nullable();
            $table->string('rfc')->comments('RFC del proveedor')->nullable();
            $table->string('email')->comments('Email del proveedor')->nullable();
            $table->boolean('sync')->comments('El proveedor ha sido sincronizado- null-sin sincronizar, 0->aceptado, 1->rechazado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
