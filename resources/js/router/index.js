import {createWebHistory, createRouter} from "vue-router";

import Login from '../pages/Login.vue';
import Dashboard from '../pages/Dashboard.vue';

import Suppliers from '../components/Suppliers/Suppliers';


export const routes = [
    {
        name: 'home',
        path: '/',
        component: Dashboard
    },
    {
        name: 'login',
        path: '/login',
        component: Login
    },
    {
        name: 'dashboard',
        path: '/dashboard',
        component: Dashboard
    },
    {
        name: 'suppliers',
        path: '/suppliers',
        component: Suppliers
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes: routes,
});

export default router;
