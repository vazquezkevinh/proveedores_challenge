
# ¡Hola! 
Soy Kevin Vázquez, desarrollador FullStack y este repositorio contiene un **Portal de proveedores**. Desarrollado en Laravel y Vue js =)

# Instalación

- Clonar el proyecto
    >git clone git@gitlab.com:vazquezkevinh/proveedores_challenge.git

- Ingresar a la carpeta del proyecto
    >**linux**: cd proveedores_challenge

- Copiar el archivo **.env.example** y nombrarlo **.env**
    >**linux**: cp .env.example .env

- Configurar la conexión a base de datos en archivo **.env**
    >DB_CONNECTION=mysql

    >DB_HOST=127.0.0.1

    >DB_PORT=3306

    >DB_DATABASE=laravel

    >DB_USERNAME=root

    >DB_PASSWORD=

- Ejecutar los siguientes comandos en orden descendente 
    > **composer install**

    > **php artisan key:generate**

    > **npm install**

    > **npm run production**

    > **php artisan migrate**

    > **php artisan db:seed**

    > **php artisan serve**
  
 - Abrir el proyecto en el navegador 
    > **Ejemplo: ** http://127.0.0.1:8000

## ¡Excelente! ahora puedes probar el proyecto =D

- Las credenciales de prueba son las siguientes: 
    > **RFC:** VAZK950519

    > **Password:** superadmin

*Nota: el archivo de ejemplo se encuentra en la raíz del proyecto, lo puedes localizar con el nombre: **suppliers.txt** solo sigue la estructura separada por **|** en el siguiente orden: Nombre, RFC, Correo y al finalizar con un **Enter o Salto de línea***

- Aquí tienes el liveview de la aplicación
    > https://proveedores-challenge.herokuapp.com/

*Nota 2: Una vez hayas aceptado la integración de un proveedor, el usuario y contraseña será el mismo RFC en mayúsculas*